# -*- coding: utf-8 -*-

from django.urls import path
from {{app_name}} import views

app_name = '{{camel_case_app_name}}'

"""
To Include URLS from this file on your Django site, add the following to the
urlpatterns list of the urls.py file in the dj directory:
    path(
        '<int:organization_id>/<int:hierarchy_id>/{{app_name}}/',
        include('{{app_name}}.urls_org_hie',
        namespace='{{app_name}}_org_hie')
        ),

In for the SSO Auth Middle ware to authenticate requests you need to add this
following to VMSAPI_CONFIG list in the dj_settings/auth_settings.py files

    {
        'link': '/{{app_name}}/',
        'regex': r'^/(?P<organization_id>\d+)/(?P<hierarchy_id>\d+)/{{app_name}}/',
        'enabled': 1,
        'exclude_by_end_of_url': [
        ]
    },
"""

urlpatterns = [
    #/ORG/HIE/{{app_name}}/user_based_{{app_name}}/
    path(
        'user_based_{{app_name}}/',
        views.UserBased{{camel_case_app_name}}.as_view(),
        name='user_based_{{app_name}}'
    ),
]
